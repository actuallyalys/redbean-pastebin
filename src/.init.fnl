
; You can put custom build scripts here.

(global entries [])
(global *mode* :sql)

(local sqlite3 (require "lsqlite3"))
; db = sqlite3.open_memory()
(print argv)
(global db (sqlite3.open (if (>= (# argv) 1) 
                           (. argv 1)
                           "db.sqlite"
                           )))

(db:exec "CREATE TABLE IF NOT EXISTS pastes
         (ID TEXT NOT NULL,
         version INTEGER NOT NULL,
         contents TEXT,
         created TEXT,
         PRIMARY KEY (ID, version));")


;Might be overkill, but setting the seed based wholly on os.time seems guessable in some circumstances
;E.g., if an attacker notices the service goes down, they could watch until it comes back up again, generate a pastebin, and then generate possible random values between the current value until they get a random ID that matches the one they were given by the server. 
(global urandom (fn []
  (var newrandom 0)
  (let [f (io.open "/dev/urandom" "rb")]
    (each [i n (ipairs [(string.byte (f:read 32) 1 -1)])]
      (set newrandom (+ newrandom (* n (^ 2 i)))))
    (f:close))
  (when (= nil newrandom) (print "Could not get random from /dev/urandom"))
  (* (if (= nil newrandom) newrandom 1) (os.time))))

; (comment )

; (math.randomseed (os.time))
(global seed (let [seed (urandom)]
  (print (.. "Setting seed to " (tostring seed)))
  (math.randomseed seed)))
