(local clj (require :cljlib))


(fn gen-attributes [attributes]
  (clj.reduce (fn [x y] (.. x y))
      (icollect [attr val (pairs attributes)]
        (string.format  " %s=\"%s\"" attr val))))

;
(fn render [form]
  (if (clj.string? form) (EscapeHtml form)
    (clj.vector? form) (let [tag (. form 1)
                            attributes (when (clj.map? (. form 2) ) (. form 2))
                             contents (if (clj.map? (. form 2)) 
                                        (-> form 
                                               clj.rest 
                                               clj.rest) 
                                        (clj.rest  form))
                            rendered-contents (clj.reduce (fn [x y] (.. x y)) 
                                                    (clj.mapv render contents))]
                         (string.format "<%s %s>%s</%s>" tag 
                                        (if attributes (gen-attributes attributes) "")
                                        rendered-contents
                                        tag))))

                            ; (clj.get (clj.get (GetParams) 2 [""]) 1 "")
        ;                      (clj.reduce (fn [x y] (.. x y))                
        ;                        (icollect [index array (ipairs  (GetParams))]
        ; (string.format  " %s=\"%s\"" (clj.get array 1 "") (clj.get array 2 ""))))

(fn exec-to-table [db statement]
  (let [newtable []]
    (each [row (db:nrows statement )]
      (table.insert newtable row))
newtable))

(fn stmt-to-table [statement]
  (let [newtable []]
    (each [row (statement:nrows )]
      (table.insert newtable row))
newtable))


(fn gen-id []

(string.format "%x-%x-%x-%x"
               (math.random 1 1e9)
               (math.random 1 1e9)
               (math.random 1 1e9)
               (math.random 1 1e9)))


; (fn get-count [db]
; (clj.take 1 (clj.seq  )))

(fn convert-error [result]
  (match result
    BUSY "database is busy" 
    DONE "request succeeded!" 
    ROW  "new row to process"
    ERROR "error in query"
    MISUSE "step was called inappropriately."))


(fn get-count [db] 
 (tostring (clj.get (clj.first (exec-to-table db "SELECT count(DISTINCT ID) AS num FROM pastes;")) "num" "N/A")))

(fn get-paste [db id] 
  (let [statement (db:prepare "SELECT contents FROM pastes WHERE id = ?;" ) ]
    (statement:bind_values id)
    (tostring (clj.get (clj.first (stmt-to-table statement)) "contents" "N/A"))))

(fn insert-paste [db id contents] 
  (let [statement (db:prepare "INSERT INTO pastes (id, version, contents) VALUES (?, 1, ?);" ) ]
    (statement:bind_values id contents)
    (Log kLogInfo id)
    (Log kLogInfo (convert-error (statement:step) )
    ;(statement:reset)
    (Log kLogInfo (convert-error (statement:finalize))))))

(local index (fn []
               ;(Log kLogInfo "Starting to render index.")
                (match [(GetMethod) (GetParam :id) (GetParam :v)]
                  ["GET" nil nil] (Write (.. "<!DOCTYPE HTML>"
                                           (render [:html
                                                [:head
                                                 [:style 
                                                  "body { width: 90%;
                                                          max-width: 900px;
                                                          margin: auto;}
                                                  textarea { width: 100%;}"]] 
                                                  [:body [:h1 "Pastebin"]
                                                   [:p "A very basic pastebin"]
                                                   [:form  {:method "post" :action ""}
                                                    [:label "Text:"]
                                                    [:textarea {:name "text"} ""]
                                                    [:input {:type "submit"} ""] ]
                                                   [:footer "Serving " (if (= *mode* :sql)
                                                                         (get-count db)
                                                                         (tostring (# entries))) " pastebins."]]])))
                  ["GET" id nil] 

                  (Write (render [:html [:body [:p "You requested id " id]
                                             
                                             [:pre ""
                                              (if  (= *mode* :sql)
                                                     (get-paste db id)
                                                     (clj.get-in entries [ (tonumber id) 1] ""))]]]))
                  ["GET" id v] (Write (render [:html [:body [:p "You requested id " id]
                                             
                                             [:pre (clj.get-in entries [ (tonumber id) v] "")] ]]))

                  ["POST" nil nil] (let [id (tostring (gen-id))]
                                 (table.insert entries [(GetParam "text")])
                                 (insert-paste db id (GetParam "text"))
                                 (Write "You wrote: <br />")
                                 (Write (render [:pre
                                           (GetParam "text") ]))
                                 (Write (render [:a {:href (.. "/?id=" id)} "Permalink"])))
                  _ (Write "Unrecognized"))
                ; (Log kLogInfo "Finished rendering index.")
                ))
;I think because of how redbean does forking, we need to reset the seed:

(fn update-seed [oldseed]
  (Log kLogInfo "Starting to set seed.")
  (let [newseed (* oldseed (urandom) (get-count db))]
    (Log kLogDebug (.. "Setting seed to " (tostring newseed)))
    (math.randomseed newseed)

    (Log kLogInfo "Finished setting seed.")
    newseed))
  
(global seed (update-seed seed))

(index)
