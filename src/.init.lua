-- While you can add to this file, I suggest changing .init.fnl instead.

HidePath('/usr/share/zoneinfo/')

local fennel = require("fennel")

function OnHttpRequest()
	if (string.find(GetPath(), ".fnl")) then
		local module = string.gsub(GetPath(), '/', '')
		fennel.dofile('zip:' .. module)
	elseif (("/" == GetPath()) and (io.open("zip:index.fnl") ~=nil)) then
		fennel.dofile('zip:index.fnl')

	else
		Route()
	end

end

if (io.open("zip:.init.fnl")) then
	fennel.dofile('zip:.init.fnl')
end
