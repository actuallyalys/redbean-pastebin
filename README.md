
Template project for creating a Redbean server with Fennel support. This means
you get a run-anywhere Lisp server with good performance in just a few megabytes.

This template creates binaries that can be run anywhere, but it requires InfoZip
and `make`, so you need to install these utilities on Windows (perhaps
via Cygwin or WSL).

This template isn't ready for serious use. Redbean and Fennel are
young but very stable in my experience, but I'm not aware of anyone else using them together.

## Usage ##


1. Either clone the repository or download a zip file to a directory of your
   choice.
2. Change `output_file` in `Makefile` to your desired app name.
3. Add Fennel, Lua, and HTML files to the `src/` directory.
4. Run `make debug` to create the file.
4. Run `make run` to run it.


### REPL ###

You can run a REPL:

    make repl

### Upgrades ###

You can upgrade Fennel and Redbean by modifying the `version` and
`fennel-version` variables in the Makefile.


## Troubleshooting ##


### `run-detectors: unable to find an interpreter` ###

According to the creator of Redbean, this happens if you have binfmt_misc enabled. Run:

    sudo sh -c "echo ':APE:M::MZqFpD::/bin/sh:' >/proc/sys/fs/binfmt_misc/register"


### `invalid file (bad magic number)` ###

I'm honestly not sure why this happens. It may be that certain errors leave the
zip file in an incorrect state. But I do know how to fix it: Run `make clean` to
delete both the dependencies and the build artifacts. Note that this will delete
any files you manually added to the redbean `.com` file and your generated
`.com` file.


## License

(c) 2021 Alys S Brooks

Licensed under the MIT license, same as Fennel and similar to Redbean (it uses
the ISC, which is nigh identical to the MIT license).

