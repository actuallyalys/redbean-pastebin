
# This makes heavy use of variables. If you find the Make variable syntax hard to read, sorry. :/
# All of the variables at the moment are either version strings or the name of your application.

version = 1.4
fennel-version = 0.9.2

output_name = out


redbean-$(version).com: 
	wget "https://redbean.dev/redbean-$(version).com"
	cp "redbean-$(version).com" "$(output_name).com"
	cp "redbean-$(version).com" "$(output_name)-prod.com"


fennel-$(fennel-version).tar.gz:
	wget "https://fennel-lang.org/downloads/fennel-$(fennel-version).tar.gz"

src/.lua/fennel.lua: fennel-$(fennel-version).tar.gz
	mkdir src/fennel
	mkdir -p src/.lua
	tar -xf fennel-$(fennel-version).tar.gz -C src/fennel
	mv src/fennel/fennel-$(fennel-version)/fennel.lua src/.lua/
	rm -r src/fennel


fennel-$(fennel-version):
	wget https://fennel-lang.org/downloads/fennel-$(fennel-version)
	chmod +x fennel-$(fennel-version)

deps: redbean-$(version).com src/.lua/fennel.lua


extras:
	wget "https://redbean.dev/redbean-$(version).com.dbg"
	wget "https://github.com/jart/cosmopolitan/blob/master/tool/net/redbean.c"

$(output_name).com: deps
	# cd src; zip ../$(output_name).com -r fennel
	cd src; zip ../$(output_name).com -r .lua
	cd src; zip ../$(output_name).com *.lua *.fnl *.htm* .init.fnl .init.lua
	chmod +x $(output_name).com


out/%.lua: src/%.fnl fennel-$(fennel-version)
	mkdir -p out/.lua
	# Adding the dash (to ignore errors) because of macro files
	-./fennel-$(fennel-version) --add-fennel-path src/.lua/cljlib/init-macros.fnl --compile $< > $@

debug: $(output_name).com

release: deps $(shell find src/ -type f -iname "*.fnl"| sed "s/.fnl/.lua/" | sed "s/src/out/")
	cd out/; zip ../$(output_name)-prod.com *.lua *.htm* .init.lua
	cd out/; zip ../$(output_name)-prod.com -r .lua
	chmod +x $(output_name)-prod.com

run:
	./$(output_name).com -vv -p 8080 -l 127.0.0.1

repl: fennel-$(fennel-version)
	rlwrap ./fennel-$(fennel-version)


clean-deps:
	rm -r redbean-$(version).com 

clean-build:
	rm $(output_name).com  $(output_name)-prod.com


clean: clean-deps clean-build
